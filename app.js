// app.js

// number of particles
var N = 75;

// particles data structure
var particles = [];
var percievedCenter = new p5.Vector(0,0);

// initialize sketch
function setup() {
	// canvas width and height
	var width = window.innerWidth;
	var height = window.innerHeight;

	var focalPoint = new p5.Vector(random(width), random(height));

	createCanvas(width, height);

	// create particles
	for (var i = 0; i <= N; i++) {
		// create a particle with random vectors
		var x = random((focalPoint.x) + 300);
		var y = random((focalPoint.y)+ 300);
		var position = new p5.Vector(x,y);
		var velocity = new p5.Vector(random(-1,1),random(-1,1));
		var acceleration = new p5.Vector(0,0);
		var id = i;

		var newParticle = new Particle(position, velocity, acceleration, i);
		particles.push(newParticle);
	}
	console.log(particles);

}

function draw() {
	clear();

	// canvas width and height
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	// if a particle moves out of bounds, reverse its velocity
	for (var i = 0; i <= N; i++) {
		if (particles[i]._position.x < 0) {
			particles[i]._position.x += width;
		}
		if (particles[i]._position.x  > width) {
			particles[i]._position.x -= width;
		}
		if (particles[i]._position.y < 0) {
			particles[i]._position.y += height;
		}
		if (particles[i]._position.y > height) {
			particles[i]._position.y -= height;
		}

	}

	// calculate the acceleration for each particle
	for (var i = 0; i < N; i++) {
		var v1 = cohesion(particles[i]);
		var v2 = separation(particles[i]);
		var v3 = alignment(particles[i]);

		var newVector = v1.add(v2).add(v3);
		
		particles[i].update(newVector);


		// render the particle
		particles[i].display();
	}
	

}