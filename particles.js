// particles.js
// implementation of a standard euclidean particle with position, velocity, and acceleration vectors

// creates a particle with three vectors: position, velocity and acceleration
function Particle(position, velocity, acceleration, id) {
	// initialize particle's position, velocity, and acceleration
	this._position = position;
	this._velocity = velocity;
	this._acceleration = acceleration;
	this._id = id;

	// update particle's position and velocity
	this.update = function(pAcceleration) {
		// limit the speed of the particle by 3 units per frame
		this._velocity.add(pAcceleration).limit(3);
		this._position.add(this._velocity);
	};

	// draw the particle
	this.display = function() {
		stroke(0);
		fill(255);
		ellipse(this._position.x, this._position.y, 5, 5);
	};
}