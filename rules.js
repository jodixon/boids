// rules.js
//
// Rules for flocking behavior described by Craig Reynolds (http://www.red3d.com/cwr/boids/)
// Psuedo-code example provided by Conrad Parker (http://www.kfish.org/boids/pseudocode.html)
// An example of the algorithm implemented in CoffeeScript provided by Harry Brundage (http://harry.me/blog/2011/02/17/neat-algorithms-flocking/)

// Rule 1. Boids try to fly toward the center of mass of neighboring boids.
function cohesion(currentBoid) {
  	// calculate the percieved center
    var mean = new p5.Vector();
    var count = 0;
    for (var i=0; i<=N; i++) {
      if (particles[i]._id != currentBoid._id) {
        var d = currentBoid._position.dist(particles[i]._position);
        if ((d > 0) && (d < 15)) {
          mean.add(particles[i]._velocity);
          count++;
        }
      }
    }
    if (count > 0) mean.div(count);
    // returns an acceleration vector
    return mean;

}

// Rule 2. Boids try to keep a small distance away from other objects (including other boids).
function separation(currentBoid) {
  var mean = new p5.Vector();
  var count = 0;
  for (var i = 0; i<=N; i++) {
    if (particles[i]._id != currentBoid._id) {
      var d = currentBoid._position.dist(particles[i]._position);
      if ((d > 0) && (d < 10)) {
        var n = currentBoid._position.sub(particles[i]._position).normalize().div(d);
        mean.add(n);
        count++;
      }
    }
  }
  if (count > 0) mean.div(count);
  // returns an acceleration vector
  return mean;
}

// // Rule 3. Boids try to match velocity with near boids.
function alignment(currentBoid) {
  var mean = new p5.Vector();
  var count = 0;
  for (var i = 0; i<=N; i++) {
    if (particles[i]._id != currentBoid._id) {
      var d = currentBoid._position.dist(particles[i]._position);
      if ((d > 0) && (d < 150)) {
        mean.add(particles[i]._velocity);
        count++;
      }
    }
  }
  if (count > 0) mean.div(count);
  // returns an acceleration vector
  return mean;
}



